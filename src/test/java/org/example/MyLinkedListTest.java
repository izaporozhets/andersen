package org.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MyLinkedListTest
{
    MyLinkedList<String> myLinkedList;

    @Before
    public void setUp(){
        myLinkedList = new MyLinkedList<>();
    }

    @Test
    public void add()
    {
        myLinkedList.add("A");
        myLinkedList.add("B");
        myLinkedList.add("C");
        assertEquals(3,myLinkedList.size());
    }

    @Test
    public void addWithIndex(){
        myLinkedList.add("A");
        myLinkedList.add("C");
        myLinkedList.add("C");
        myLinkedList.add(1,"B");
        assertEquals("B",myLinkedList.get(1));
    }

    @Test
    public void get(){
        myLinkedList.add("1");
        myLinkedList.add("2");
        myLinkedList.add("3");
        myLinkedList.add("4");
        assertEquals("3",myLinkedList.get(2));
    }

    @Test
    public void removeWithIndex(){
        myLinkedList.add("One");
        myLinkedList.add("Two");
        myLinkedList.add("Two");
        myLinkedList.add("Three");
        int size= myLinkedList.size();
        assertEquals("Two",myLinkedList.remove(1));
        assertEquals(size-1,myLinkedList.size());
        assertEquals("One Two Three ", myLinkedList.toString());
    }

    @Test
    public void removeByValue(){
        myLinkedList.add("This");
        myLinkedList.add("is");
        myLinkedList.add("the");
        myLinkedList.add("a");
        myLinkedList.add("message");
        assertTrue(myLinkedList.remove("a"));
        assertEquals("This is the message ", myLinkedList.toString());

        myLinkedList.remove("This");
        myLinkedList.remove("is");
        myLinkedList.remove("the");
        myLinkedList.remove("message");
        assertEquals(0, myLinkedList.size());
    }

    @Test
    public void addFirstMethod(){
        myLinkedList.addFirst("C");
        myLinkedList.addFirst("B");
        myLinkedList.addFirst("A");
        assertEquals("A B C ",myLinkedList.toString());
    }

    @After
    public void clear(){
        myLinkedList.clear();
    }

}
