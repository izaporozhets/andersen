package org.example;
import java.util.*;

public class MyLinkedList<T> implements List<T> {

    private static class Node<T> {
        T data;
        Node<T> next;

        Node(T data) {
            this.data = data;
        }
    }

    private Node<T> head;
    private int size = 0;

    public MyLinkedList() {
        head = null;
    }

    public void addFirst(T t) {
        Node<T> first = new Node<>(t);
        first.next = head;
        head = first;
    }

    public void addLast(T t) {
        if (head == null) {
            head = new Node<>(t);
            ++size;
            return;
        }
        Node<T> current = head;
        while (current.next != null) {
            current = current.next;
        }
        current.next = new Node<>(t);
        ++size;
    }

    @Override
    public boolean add(T t) {
        addLast(t);
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T get(int index) {
        if (head == null || index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index : " + index + ", Size:" + size);
        }
        if(index == 0){
            return head.data;
        }
        int counter = 0;
        Node<T> current = head;
        while (current.next != null) {
            current = current.next;
            ++counter;
            if (index == counter) {
                return current.data;
            }
        }
        return null;
    }

    @Override
    public void add(int index, T element) {
        if (head == null || index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index : " + index + ", Size:" + size);
        }
        int counter = 0;
        Node<T> current = head;
        while (current.next != null) {
            current = current.next;
            ++counter;
            if (index == counter) {
                current.data = element;
                return;
            }
        }
    }

    @Override
    public T remove(int index) {
        if (head == null || index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index : " + index + ", Size:" + size);
        }
        if(index == 0){
            Node<T> tmp = head;
            head = head.next;
            size--;
            return tmp.data;
        }
        Node<T> current = head;
        Node<T> prev;
        int counter = 0;

        while (current.next != null) {
            prev = current;
            current = current.next;
            ++counter;
            if (counter == index) {
                prev.next = current.next;
                size--;
                return current.data;
            }
        }
        return null;
    }

    @Override
    public boolean remove(Object o) {
        if (head == null) {
            throw new NullPointerException("No such element in list");
        }
        if (head.data.equals(o)) {
            head = head.next;
            size--;
            return true;
        }
        Node<T> current = head;
        while (current.next != null) {
            if (current.next.data.equals(o)) {
                current.next = current.next.next;
                size--;
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (head == null) return result.toString();
        Node<T> current = head;
        while (current != null) {
            result.append(current.data).append(" ");
            current = current.next;
        }
        return result.toString();
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

}